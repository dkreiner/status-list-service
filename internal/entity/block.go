package entity

import "fmt"

var ErrFullyAllocated = fmt.Errorf("block is already fully allocated")

type Block struct {
	BlockId int
	Block   []byte
	Free    int
}

func NewBlock(blockSizeInBytes int) *Block {
	blockBitSize := blockSizeInBytes * 8
	newBinaryBlock := make([]byte, blockSizeInBytes)

	for i := range newBinaryBlock {
		newBinaryBlock[i] ^= 255
	}

	return &Block{
		BlockId: 0,
		Block:   newBinaryBlock,
		Free:    blockBitSize,
	}
}

func (b *Block) RevokeAtIndex(index int) {
	byteIndex, bitIndex := index/8, index%8
	b.Block[byteIndex] = b.Block[byteIndex] &^ (1 << (7 - bitIndex))
}

func (b *Block) AllocateNextFreeIndex() (index int, err error) {
	if b.Free > 0 {
		index = (len(b.Block) * 8) - b.Free
		b.Free--
	} else {
		err = ErrFullyAllocated
	}

	return index, err
}
