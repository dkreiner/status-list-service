package config

import (
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/config"
	pgPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/db/postgres"
)

type statusListConfiguration struct {
	config.BaseConfig `mapstructure:",squash"`
	Database          pgPkg.Config `mapstructure:"database"`
	CreationTopic     string       `mapstructure:"creationTopic"`
	BlockSizeInBytes  int          `mapstructure:"blockSizeInBytes"`
}

var CurrentStatusListConfig statusListConfiguration

func Load() error {
	return config.LoadConfig("CONFIGURATIONSERVICE", &CurrentStatusListConfig, getDefaults())
}

func getDefaults() map[string]any {
	return map[string]any{
		"isDev":         false,
		"creationTopic": "status",
	}
}
